;;; Copyright 2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (test-simple-iterators)
  #:use-module (srfi srfi-64)
  #:use-module (simple-iterators))

(test-begin "test-simple-iterators")

(test-equal "\
FEE!
FI!
FO!
FUM!\n"
  (with-output-to-string
    (lambda ()
      (for (word '(fee fi fo fum))
        (format #t "~a!\n"
                (string-upcase (symbol->string word)))))))

(test-equal '("FEE!" "FI!" "FO!" "FUM!")
  (for/map (word '(fee fi fo fum))
    (format #f "~a!"
            (string-upcase (symbol->string word)))))

(test-equal "\
FEE!
FI!
FO!
FUM!\n"
  (with-output-to-string
    (lambda ()
      (for/c ((lst '(fee fi fo fum)) (cdr lst) (null? lst))
        (define word (car lst))
        (format #t "~a!\n"
                (string-upcase (symbol->string word)))))))

(test-equal "\
FEE!
FI!
FO!
FUM!\n"
  (with-output-to-string
    (lambda ()
      (let* ((words '#(fee fi fo fum))
             (words-len (vector-length words)))
        (for/c ((i 0) (+ i 1) (= i words-len))
          (define word (vector-ref words i))
          (format #t "~a!\n"
                  (string-upcase (symbol->string word))))))))

(test-equal "\
1 little monkey, jumping on the bed!
2 little monkeys, jumping on the bed!
3 little monkeys, jumping on the bed!
4 little monkeys, jumping on the bed!
5 little monkeys, jumping on the bed!\n"
  (with-output-to-string
    (lambda ()
      (for/c ((i 1) (+ i 1) (= i 6))
        (format #t "~a little ~a, jumping on the bed!\n"
                i (if (= i 1) "monkey" "monkeys"))))))

(define* (sum-even-words-vector-1 words-vec #:optional (start 0))
  (define words-len (vector-length words-vec))
  (define (proc i result)
    (define word (vector-ref words-vec i))
    (define word-len (string-length word))
    (if (even? word-len)
        (+ word-len result)
        result))
  (define (next i) (+ i 1))
  (define (stop? i) (= i words-len))
  (folder proc start 0 next stop?))

(define veggie-vec '#("carrot" "apple"
                      "pea" "potato"
                      "rutabaga"))

(test-equal 20
  (sum-even-words-vector-1 veggie-vec))

(test-equal 25
  (sum-even-words-vector-1 veggie-vec 5))

(define* (sum-even-words-vector-2 words-vec
                                  #:optional (start 0))
  (define words-len (vector-length words-vec))
  (for/folder ((i 0) (+ i 1) (= i words-len)
               (result start))
    (define word (vector-ref words-vec i))
    (define word-len (string-length word))
    (if (even? word-len)
        (+ word-len result)
        result)))

(test-equal 20
  (sum-even-words-vector-2 veggie-vec))

(test-equal 25
  (sum-even-words-vector-2 veggie-vec 5))

(test-equal 20
  (for/folder ((i 0) (+ i 1) (= i (vector-length veggie-vec))
               (result 0))
    (define word (vector-ref veggie-vec i))
    (define word-len (string-length word))
    (if (even? word-len)
        (+ word-len result)
        result)))

(test-end "test-simple-iterators")
